const Services = require('../Services')
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queuefindOne, queueUpdate } = require('./index');

queueView.process(async function(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.View({ });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueView', error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
});

queueCreate.process(async function(job, done) {

    try {

        const { age, color, name } = job.data;

        let { statusCode, data, message } = await Services.Create({ age, color, name });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueCreate', error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
});

queueDelete.process(async function(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueDelete', error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
});

queuefindOne.process(async function(job, done) {

    try {

        const { name } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ name });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueFindOne', error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
});

queueUpdate.process(async function(job, done) {

    try {

        const { age, color, name, id } = job.data;

        let { statusCode, data, message } = await Services.Update({ age, color, name, id});

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueUpdate', error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
});
