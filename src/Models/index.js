const { sequelize } = require('../settings');
const { DataTypes } = require('../sequelize');

const Model = sequelize.define('curso', {

    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING },
})

async function SyncDB() {
    try {
        await Model.sync()
    } catch (error) {
        console.log(error)
    }
}

/*const db = [
    {
        id: 1,
        info: { name: 'Enel Villafranca', edad: 33 },
        color: 'negro',
    },

    {
        id: 2,
        info: {
            name: 'Wilfredo',
            edad: 30
        },
        color: 'negro',
    },

    {
        id: 3,
        info: {
            name: 'Rincones',
            edad: 34
        },
        color: 'negro',
    }
]

*/

module.exports = { SyncDB, Model }