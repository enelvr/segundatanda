const Controller = require('../Controllers')

async function Create({ age, color, name }) {
    try {

        let { statusCode, data, message } = await Controllers.Create({age, color, name})
       
        return { statusCode, data, message }
        

    } catch (error) {
        console.log({ step: 'service Create', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }
}

async function Delete({id }) {
    try {

        let { statusCode, data, message } = await Controllers.Delete({ where: { id } })
       
        return { statusCode, data, message }
        

    } catch (error) {
        console.log({ step: 'service Delete', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }
}

async function Update({ age, color, name, id }) {
    try {

        let { statusCode, data, message } = await Controllers.Update({ age, color, name, id })
       
        return { statusCode, data, message }
        

    } catch (error) {
        console.log({ step: 'service Create', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }
}

async function FindOne({ name }) {
    try {

        let { statusCode, data, message } = await Controllers.FindOne({ name })
       
        return { statusCode, data, message }
        

    } catch (error) {
        console.log({ step: 'service FindOne', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }
}

async function View({ }) {
    try {

        let { statusCode, data, message } = await Controllers.View({ })
       
        return { statusCode, data, message }
        

    } catch (error) {
        console.log({ step: 'service View', error: error.toString() })
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { Create, Delete, Update, FindOne, View }