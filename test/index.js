const { 
    queueView,
    queueCreate,
    queueDelete,
    queuefindOne,
    queueUpdate 
} = require('../src/Adapters/index');

async function Create() {

    try {

        const job = queueCreate.add({ age, color, name })

        const result = await job.finished()

        console.log(result)

        if(result.statusCode === 200) console.log('Solicitud Correcta');
    } catch (error) {
        console.log(error)
    }
}

async function Delete() {

    try {

        const job = queueDelete.add({ id })

        const result = await job.finished()

        console.log(result)

        if(result.statusCode === 200) console.log('Solicitud Correcta');
    } catch (error) {
        console.log(error)
    }
}

async function FindOne() {

    try {

        const job = queueFindOne.add({ name })

        const result = await job.finished()

        console.log(result)

        if(result.statusCode === 200) console.log('Solicitud Correcta');
    } catch (error) {
        console.log(error)
    }
}

async function Update() {

    try {

        const job = queueUpdate.add({ age, color, name, id })

        const result = await job.finished()

        console.log(result)

        if(result.statusCode === 200) console.log('Solicitud Correcta');
    } catch (error) {
        console.log(error)
    }
}

async function View() {

    try {

        const job = queueView.add({})

        const result = await job.finished()

        console.log(result)

        if(result.statusCode === 200) console.log('Solicitud Correcta');
    } catch (error) {
        console.log(error)
    }
}

async function main() {
    View()
    Delete()
    FindOne()
    Update()
    View()
}